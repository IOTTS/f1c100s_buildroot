#!/bin/bash

./output/host/bin/sunxi-fel -p \
	uboot output/images/u-boot-sunxi-with-spl.bin \
	write 0x80008000 output/images/zImage \
	write 0x80C00000 output/images/suniv-f1c100s-licheepi-nano.dtb \
	write 0x80D00000 output/images/rootfs.cpio.uboot
